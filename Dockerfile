FROM python:3.10

COPY pyproject.toml /src/
COPY examples /examples
COPY py_object_detection_worker /src/py_object_detection_worker

WORKDIR /src

RUN apt-get update && apt-get install libgl1 -y \ 
  && wget https://pjreddie.com/media/files/yolov3.weights -P py_object_detection_worker/models \
  && pip install . && rm -rf /src/

ENV AMQP_QUEUE=job_object_detection
CMD py_object_detection_worker
