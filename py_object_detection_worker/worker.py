"""
Object detection worker based on YoloV3 and coco dataset.
It performs a frame by frame analysis and return detected objects, with their confidence and bounding boxes.
"""
from typing import Optional

import cv2
import mcai_worker_sdk as mcai
import numpy as np
import pkg_resources


class ObjectDetectionParameters(mcai.WorkerParameters):
    source_path: str = None
    destination_path: str = None
    batch_size: int
    confidence: Optional[int] = 1
    source_paths: Optional[list[str]] = None
    requirements: Optional[dict] = None


class ObjectDetectionWorker(mcai.Worker):
    knowledge_network = None
    yolo_labels = None
    confidence = None
    layer_names = None

    def setup(self) -> None:
        """
        Load the pre-trained Yolo model and store it.
        """
        labels_path = pkg_resources.resource_filename(
            "py_object_detection_worker", "models/coco.names"
        )
        config_path = pkg_resources.resource_filename(
            "py_object_detection_worker", "models/yolov3.cfg"
        )
        weights_path = pkg_resources.resource_filename(
            "py_object_detection_worker", "models/yolov3.weights"
        )

        with open(labels_path, encoding="utf-8") as labels:
            self.yolo_labels = labels.read().strip().split("\n")

        self.knowledge_network = cv2.dnn.readNetFromDarknet(config_path, weights_path)

        layer_names = self.knowledge_network.getLayerNames()
        self.layer_names = [
            layer_names[i - 1] for i in self.knowledge_network.getUnconnectedOutLayers()
        ]

    def init_process(self, context, parameters: ObjectDetectionParameters) -> list:
        """
        Select the stream we are going to perform objet detection on and apply a filter.
        """
        video_filter = mcai.Filter(name="format", label="format_filter")
        video_filter.add_parameters(pix_fmts="rgb24")

        video_stream = mcai.VideoStreamDescriptor(
            context.streams[0].index, [video_filter]
        )

        self.confidence = parameters.confidence / 100

        # returns a list of description of the streams to be processed
        return [video_stream]

    def process_frames(self, _job_id, _stream_index, frames):
        """
        Frame by frame processing: detect objects and returns detected objects and its bounding boxes.
        """
        assert len(frames) == 1
        frame = frames[0]

        img_np = np.fromstring(frame.data[0], dtype=np.uint8).reshape(
            (3, frame.width, frame.height), order="F"
        )
        img_np = np.swapaxes(img_np, 0, 2)

        blob = cv2.dnn.blobFromImage(
            img_np, 1 / 255.0, (512, 512), swapRB=False, crop=False
        )

        self.knowledge_network.setInput(blob)

        layer_outputs = self.knowledge_network.forward(self.layer_names)
        detected_objects = []

        # loop over each of the layer outputs
        for layer_output in layer_outputs:
            # loop over each of the detections
            for detection in layer_output:
                # extract the class ID and confidence (i.e., probability) of
                # the current object detection
                scores = detection[5:]
                class_id = np.argmax(scores)
                object_confidence = scores[class_id]
                # filter out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if object_confidence > self.confidence:
                    # scale the bounding box coordinates back relative to the
                    # size of the image, keeping in mind that YOLO actually
                    # returns the center (x, y)-coordinates of the bounding
                    # box followed by the boxes' width and height
                    box = detection[0:4] * np.array(
                        [frame.width, frame.height, frame.width, frame.height]
                    )

                    (center_x, center_y, width, height) = box.astype("int")
                    # use the center (x, y)-coordinates to derive the top and
                    # and left corner of the bounding box
                    top_left = {
                        "x": int(center_x - (width / 2)),
                        "y": int(center_y - (height / 2)),
                    }

                    detected_objects.append(
                        {
                            "label": self.yolo_labels[class_id],
                            "confidence": float(object_confidence),
                            "box": [
                                top_left["x"],
                                top_left["y"],
                                int(width),
                                int(height),
                            ],
                        }
                    )

        return detected_objects


def main():
    worker_description = mcai.WorkerDescription(__package__)
    worker = ObjectDetectionWorker(ObjectDetectionParameters, worker_description)
    worker.start()


if __name__ == "__main__":
    main()
